@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <!-- Success message on delete Company  -->
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h1 class="float-left">Companies</h1>
                        <a class="btn btn-outline-success float-right"
                           href="/companies/create" role="button">Add new Company</a>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">logo</th>
                                <th scope="col">Name</th>
                                <th scope="col">Website</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <th scope="row">{{$company->id}}</th>
                                    <td><img src="{{asset($company->logo ?? 'storage/no-logo.png')}}" alt="logo"
                                        style="max-height:50px;">
                                    </td>
                                    <td>{{$company->name}}</td>
                                    <td><a href="{{'http://'.$company->website ?? ''}}">{{$company->website ?? ''}}</a></td>
                                    <td>
                                        <a class="btn btn-outline-dark btn-sm"
                                           href="/companies/{{$company->id}}" role="button">show</a>

                                        <a class="btn btn-outline-success btn-sm"
                                           href="/companies/{{$company->id}}/edit" role="button">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $companies->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
