-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: laravel_test
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (4,'Oberbrunner-Doyle','merritt.schinner@example.com',NULL,'www.oberbrunnerdoyle.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(5,'Rice, Nitzsche and Mertz','flavio.mayert@example.org',NULL,'www.ricenitzscheandmertz.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(6,'Lemke-Schinner','saltenwerth@example.net',NULL,'www.lemkeschinner.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(7,'O\'Hara, Abernathy and Auer','lwiza@example.com',NULL,'www.oharaabernathyandauer.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(8,'Kiehn, Denesik and Goyette','ullrich.alia@example.com',NULL,'www.kiehndenesikandgoyette.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(9,'Fay PLC','cleo21@example.com',NULL,'www.fayplc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(10,'Herzog PLC','elliott19@example.net',NULL,'www.herzogplc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(11,'Davis-Baumbach','marion97@example.com',NULL,'www.davisbaumbach.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(12,'Russel Group','wallace48@example.net',NULL,'www.russelgroup.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(13,'Moore, Weimann and Bechtelar','sjacobs@example.net',NULL,'www.mooreweimannandbechtelar.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(14,'Kunde, Flatley and Reichert','brycen79@example.net',NULL,'www.kundeflatleyandreichert.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(15,'Hauck LLC','kutch.nickolas@example.org',NULL,'www.hauckllc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(16,'Breitenberg Group','ted74@example.net',NULL,'www.breitenberggroup.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(17,'McCullough-Hintz','pfritsch@example.net',NULL,'www.mcculloughhintz.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(18,'Effertz and Sons','huels.scot@example.org',NULL,'www.effertzandsons.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(19,'Breitenberg, Frami and Marks','trevor57@example.net',NULL,'www.breitenbergframiandmarks.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(20,'Emard-Sawayn','stanton.mazie@example.com',NULL,'www.emardsawayn.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(21,'Swaniawski PLC','hillard24@example.net',NULL,'www.swaniawskiplc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(22,'Wolff Inc','pfannerstill.madge@example.com',NULL,'www.wolffinc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(23,'Shanahan, Ullrich and Christiansen','kylee.kshlerin@example.com',NULL,'www.shanahanullrichandchristiansen.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(24,'Jacobson, Howell and Towne','pbrakus@example.com',NULL,'www.jacobsonhowellandtowne.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(25,'Graham, Stokes and Walker','estel09@example.com',NULL,'www.grahamstokesandwalker.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(26,'Rippin, Zulauf and Lebsack','rosenbaum.cecilia@example.org',NULL,'www.rippinzulaufandlebsack.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(27,'Abshire, Thiel and Murphy','cedrick53@example.net',NULL,'www.abshirethielandmurphy.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(28,'Flatley-Grant','ava.mcclure@example.net',NULL,'www.flatleygrant.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(29,'Stark, Donnelly and Anderson','jgleason@example.com',NULL,'www.starkdonnellyandanderson.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(30,'Mitchell LLC','elliot.watsica@example.net',NULL,'www.mitchellllc.com','2020-02-19 12:21:51','2020-02-19 12:21:51'),(31,'Papaya','papaya@papa.com','storage/KKXWNYTlRbzvNW14qy7yklRk0Ql9YN5ZZVRV4Wp9.jpeg','www.papaya.com','2020-02-19 17:25:35','2020-02-19 17:25:35'),(32,'Nike','nike@nike.com','storage/Y5IZsj5qnc2DnM02T2ieQzZEaEHWUdnmcPY0mBi6.jpeg','nike.com','2020-02-19 17:43:02','2020-02-19 17:43:02'),(33,'adidas','adidas@adiadas.com','storage/7wlOiyWT7KlYwgWMkv92KOId3ZZ2FPvt1r7gmpiH.png',NULL,'2020-02-19 17:48:43','2020-02-19 17:48:43'),(34,'Random Company','ranco@aaa.com','storage/eSM9g03bsrT0OkoLqPvGpibj3LMGbuanVB4RpCJe.jpeg','random.com','2020-02-20 06:46:12','2020-02-20 06:49:11'),(35,'bio-bio','bio@bio.com','storage/uk8cgm4flznqbiA42TrLpUp79zszsbBASPN1hf0O.jpeg','www.bio.com','2020-02-20 09:38:49','2020-02-20 09:39:01');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (2,'Lorine Murazik','Grimes',21,'ikoss@example.org','1-679-495-4956 x7800','2020-02-19 12:21:51','2020-02-19 12:21:51'),(3,'Mrs. Daphne Legros Jr.','Wilkinson',25,'jayde.king@example.net','+1-858-660-4032','2020-02-19 12:21:51','2020-02-19 12:21:51'),(4,'Mattie Glover','Stamm',26,'darrick.walker@example.org','(368) 804-3545 x280','2020-02-19 12:21:51','2020-02-19 12:21:51'),(5,'Reuben Ernser','Streich',24,'murray.camron@example.net','1-336-664-1271 x343','2020-02-19 12:21:51','2020-02-19 12:21:51'),(6,'Ms. June Denesik II','VonRueden',5,'dhermann@example.net','858-676-4146','2020-02-19 12:21:51','2020-02-19 12:21:51'),(7,'Prof. Waylon Harvey DVM','Schuppe',20,'hane.luz@example.org','571.341.3881','2020-02-19 12:21:51','2020-02-19 12:21:51'),(8,'Lurline Macejkovic','Bergnaum',1,'mbeahan@example.com','702-560-2088','2020-02-19 12:21:51','2020-02-19 12:21:51'),(9,'Mrs. Adriana Bradtke I','Kilback',7,'guadalupe41@example.com','575-394-4772','2020-02-19 12:21:51','2020-02-19 12:21:51'),(10,'Donnie Pagac','Osinski',3,'lbechtelar@example.org','1-929-690-9777 x000','2020-02-19 12:21:51','2020-02-19 12:21:51'),(11,'Dr. Evert Hane','Schuppe',18,'freddy25@example.net','+1-580-967-6785','2020-02-19 12:21:51','2020-02-19 12:21:51'),(12,'Madelynn Jones','Hagenes',18,'andres.hoeger@example.org','1-934-802-0916 x358','2020-02-19 12:21:51','2020-02-19 12:21:51'),(13,'Dr. Roma Larson IV','Herzog',25,'bo.morissette@example.org','774.613.9738 x104','2020-02-19 12:21:51','2020-02-19 12:21:51'),(14,'Litzy Kshlerin','Friesen',7,'lucienne.lang@example.com','1-748-812-6430','2020-02-19 12:21:51','2020-02-19 12:21:51'),(15,'Nicolas Wiegand','Blick',26,'leif.miller@example.net','(687) 208-0749 x32542','2020-02-19 12:21:51','2020-02-19 12:21:51'),(16,'Prof. Frankie Spencer','Satterfield',29,'nienow.emmy@example.org','(540) 990-5410 x39381','2020-02-19 12:21:51','2020-02-19 12:21:51'),(17,'Myah Little','Ryan',24,'brandy.thiel@example.net','(283) 393-8009 x011','2020-02-19 12:21:51','2020-02-19 12:21:51'),(18,'Jerome Yost','Metz',19,'larue.windler@example.org','(748) 447-9812','2020-02-19 12:21:51','2020-02-19 12:21:51'),(19,'Emilie Simonis','Rosenbaum',6,'lonny05@example.net','+1-360-457-4693','2020-02-19 12:21:51','2020-02-19 12:21:51'),(20,'Triston Baumbach','Rodriguez',24,'lily.murray@example.net','305-306-2254 x75337','2020-02-19 12:21:51','2020-02-19 12:21:51'),(21,'Gunnar Zieme IV','Howell',28,'wehner.dortha@example.org','606-702-9258 x7772','2020-02-19 12:21:51','2020-02-19 12:21:51'),(22,'Mrs. Estella Hahn II','Russel',15,'richmond.corwin@example.org','1-670-384-0528 x4182','2020-02-19 12:21:51','2020-02-19 12:21:51'),(23,'Austen Kub','Conn',21,'loren.leffler@example.net','(621) 261-2541 x9846','2020-02-19 12:21:51','2020-02-19 12:21:51'),(24,'Prof. Estella Moen','Schaefer',10,'maxwell52@example.com','+1-462-851-5365','2020-02-19 12:21:51','2020-02-19 12:21:51'),(25,'Prof. Laura Kshlerin','Bogan',18,'irwin28@example.org','480-262-6479 x714','2020-02-19 12:21:51','2020-02-19 12:21:51'),(26,'Prof. Aliya Kihn IV','Lebsack',23,'ahuel@example.org','(348) 495-0236 x9561','2020-02-19 12:21:51','2020-02-19 12:21:51'),(27,'Otis Beatty','Beer',9,'jarred.deckow@example.org','991-526-4430 x94021','2020-02-19 12:21:51','2020-02-19 12:21:51'),(28,'Lauretta Little','Zulauf',1,'wuckert.maddison@example.org','1-753-639-2453 x896','2020-02-19 12:21:51','2020-02-19 12:21:51'),(29,'Prof. Keegan Rath V','Lueilwitz',7,'glarson@example.com','283.359.1800','2020-02-19 12:21:51','2020-02-19 12:21:51'),(30,'Marisa Ziemann I','Von',27,'barrows.hilma@example.org','1-326-247-3904','2020-02-19 12:21:51','2020-02-19 12:21:51'),(31,'Dr. Alden Raynor Sr.','Hansen',21,'strosin.madalyn@example.org','826.581.9063 x24210','2020-02-19 12:21:51','2020-02-19 12:21:51'),(32,'Alize Rodriguez','Kling',10,'mjaskolski@example.com','1-979-880-4483','2020-02-19 12:21:51','2020-02-19 12:21:51'),(33,'Liliana Volkman','McKenzie',11,'oreilly.chelsie@example.com','(657) 776-1038 x8617','2020-02-19 12:21:51','2020-02-19 12:21:51'),(34,'Marcia Hintz DDS','Hirthe',24,'deshawn91@example.org','939-702-3552 x8719','2020-02-19 12:21:51','2020-02-19 12:21:51'),(35,'Noble Bergstrom','Aufderhar',18,'daniel.zack@example.org','(347) 883-4543 x9903','2020-02-19 12:21:51','2020-02-19 12:21:51'),(36,'Jamel Hartmann II','Cassin',25,'ysipes@example.org','1-749-537-2879 x7124','2020-02-19 12:21:51','2020-02-19 12:21:51'),(37,'Brendan Ruecker','Ruecker',14,'sandrine14@example.org','(506) 267-1209 x321','2020-02-19 12:21:51','2020-02-19 12:21:51'),(38,'Prof. Mac Ritchie II','Wisozk',5,'bogisich.maddison@example.net','(373) 279-7143','2020-02-19 12:21:51','2020-02-19 12:21:51'),(39,'Dr. Steve Friesen II','Spinka',5,'rkertzmann@example.net','679.617.4015 x61744','2020-02-19 12:21:51','2020-02-19 12:21:51'),(40,'Abigayle Doyle','Abshire',30,'hand.reggie@example.org','1-636-478-5611','2020-02-19 12:21:52','2020-02-19 12:21:52'),(41,'Britney Hane','Williamson',24,'lockman.julie@example.net','825-967-5653 x705','2020-02-19 12:21:52','2020-02-19 12:21:52'),(42,'Willis Batz','Hauck',7,'walter.yost@example.com','1-427-692-9484','2020-02-19 12:21:52','2020-02-19 12:21:52'),(43,'Prof. Moses Spinka I','Larson',3,'kemmerich@example.net','353.996.1378 x527','2020-02-19 12:21:52','2020-02-19 12:21:52'),(44,'Carey Kling','Runte',25,'kuhic.jessyca@example.com','+15822726739','2020-02-19 12:21:52','2020-02-19 12:21:52'),(45,'Gust Altenwerth','Rutherford',14,'heathcote.benjamin@example.org','1-608-682-2812','2020-02-19 12:21:52','2020-02-19 12:21:52'),(46,'Gerardo Herzog PhD','Hamill',5,'barrett64@example.org','995-403-6591','2020-02-19 12:21:52','2020-02-19 12:21:52'),(47,'Geovanni Fahey','Carroll',24,'herminio.ebert@example.org','1-529-927-5791','2020-02-19 12:21:52','2020-02-19 12:21:52'),(48,'Doris Bins','Tremblay',6,'champlin.emanuel@example.org','534.745.0923 x92390','2020-02-19 12:21:52','2020-02-19 12:21:52'),(49,'Brandyn Ernser','Mills',25,'andrew.fadel@example.com','906-749-3758 x637','2020-02-19 12:21:52','2020-02-19 12:21:52'),(50,'Billie Hand','Cummerata',17,'qwatsica@example.com','+12713544906','2020-02-19 12:21:52','2020-02-19 12:21:52'),(51,'Frieda Bahringer','Pfeffer',13,'howell.meagan@example.com','629-297-9386 x3857','2020-02-19 12:21:52','2020-02-19 12:21:52'),(52,'Brando Luettgen','Skiles',8,'cfriesen@example.com','741-726-8343','2020-02-19 12:21:52','2020-02-19 12:21:52'),(53,'Dr. Ottilie Hessel','Runolfsdottir',6,'paula36@example.net','754.669.5877','2020-02-19 12:21:52','2020-02-19 12:21:52'),(54,'Alycia Abernathy','Kiehn',27,'price05@example.org','345-980-9671 x3467','2020-02-19 12:21:52','2020-02-19 12:21:52'),(55,'Dr. Chelsea Mraz','Kovacek',19,'khintz@example.org','(472) 666-9324 x59091','2020-02-19 12:21:52','2020-02-19 12:21:52'),(56,'Marvin Hermiston Jr.','Morar',27,'morar.jerry@example.org','685-966-6638','2020-02-19 12:21:52','2020-02-19 12:21:52'),(57,'Emmett Mertz','Kiehn',29,'klindgren@example.org','+1-595-333-9583','2020-02-19 12:21:52','2020-02-19 12:21:52'),(58,'Dorian Cartwright Jr.','Mayer',23,'price.magnolia@example.net','(238) 747-0803 x7419','2020-02-19 12:21:52','2020-02-19 12:21:52'),(59,'Prof. Hugh Hackett','O\'Reilly',19,'lindgren.aylin@example.org','1-776-964-9045 x774','2020-02-19 12:21:52','2020-02-19 12:21:52'),(60,'Dr. Kaia McKenzie IV','Lueilwitz',28,'gnienow@example.net','(245) 558-6485','2020-02-19 12:21:52','2020-02-19 12:21:52'),(61,'Susana Beatty','Wisoky',26,'cummerata.tiara@example.org','+18854806448','2020-02-19 12:21:52','2020-02-19 12:21:52'),(63,'Palma Borer','Romaguera',13,'carleton67@example.org','(792) 859-6846 x62329','2020-02-19 12:21:52','2020-02-19 12:21:52'),(64,'Willis Jenkins PhD','Kutch',2,'maddison04@example.org','1-578-596-3478 x2322','2020-02-19 12:21:52','2020-02-19 12:21:52'),(65,'Prof. Baby Christiansen','Cassin',20,'gleason.schuyler@example.org','(650) 649-6102','2020-02-19 12:21:52','2020-02-19 12:21:52'),(66,'Laurie Wehner','Ferry',24,'arch.powlowski@example.org','+1 (889) 863-7194','2020-02-19 12:21:52','2020-02-19 12:21:52'),(67,'Rylee Monahan','O\'Connell',24,'marcia86@example.org','+1-578-538-4336','2020-02-19 12:21:52','2020-02-19 12:21:52'),(68,'Kenton Ondricka IV','Cummings',14,'emanuel46@example.net','303.890.7010 x76000','2020-02-19 12:21:52','2020-02-19 12:21:52'),(69,'Annamae Kovacek IV','Bradtke',25,'ziemann.jacynthe@example.net','(589) 560-6337','2020-02-19 12:21:52','2020-02-19 12:21:52'),(70,'Prof. Savanna Senger','Schuster',2,'mlubowitz@example.org','471-807-1026','2020-02-19 12:21:52','2020-02-19 12:21:52'),(71,'Ms. Clementina Hackett Sr.','Predovic',19,'reagan.koelpin@example.org','(236) 459-9054 x707','2020-02-19 12:21:52','2020-02-19 12:21:52'),(72,'Alanis Streich MD','Altenwerth',3,'clifford.brown@example.com','+1-825-441-0066','2020-02-19 12:21:52','2020-02-19 12:21:52'),(73,'Margarita Christiansen','Schinner',5,'layne89@example.com','(769) 414-0554 x631','2020-02-19 12:21:52','2020-02-19 12:21:52'),(74,'Cecile Moen','Hauck',20,'mrohan@example.com','214.930.7790 x1615','2020-02-19 12:21:52','2020-02-19 12:21:52'),(75,'Nichole Lakin','Corwin',18,'pearlie31@example.com','926-234-2864','2020-02-19 12:21:52','2020-02-19 12:21:52'),(76,'Gabriella Wintheiser','Breitenberg',4,'hettinger.sandrine@example.com','+18103410122','2020-02-19 12:21:52','2020-02-19 12:21:52'),(77,'Bart Wilkinson','Rogahn',7,'retta.hagenes@example.net','(536) 376-3613 x48034','2020-02-19 12:21:52','2020-02-19 12:21:52'),(78,'Kaleigh Gislason','Bahringer',11,'clemmie19@example.com','735-884-9953 x30667','2020-02-19 12:21:52','2020-02-19 12:21:52'),(79,'Greta Rutherford IV','Green',12,'amina.stroman@example.org','+1-759-272-2113','2020-02-19 12:21:52','2020-02-19 12:21:52'),(80,'Leonel Conroy','Kautzer',7,'idell94@example.com','+1 (742) 904-6952','2020-02-19 12:21:52','2020-02-19 12:21:52'),(81,'Dion Williamson II','Marks',25,'elwin.langworth@example.com','242.582.2514 x6186','2020-02-19 12:21:52','2020-02-19 12:21:52'),(82,'Janae Harvey','Gorczany',22,'golda.parisian@example.com','(305) 551-3384 x783','2020-02-19 12:21:52','2020-02-19 12:21:52'),(83,'Elyssa Bahringer','Lubowitz',14,'orie.cremin@example.com','1-658-445-5112 x8403','2020-02-19 12:21:52','2020-02-19 12:21:52'),(84,'Savanna Botsford','Heller',3,'alysa.kuphal@example.com','1-921-653-8762','2020-02-19 12:21:52','2020-02-19 12:21:52'),(85,'Fredrick Howe I','Champlin',18,'aditya.grant@example.org','789.387.6940 x62202','2020-02-19 12:21:52','2020-02-19 12:21:52'),(86,'Lincoln Mertz DDS','Steuber',18,'alexane.shanahan@example.org','330.831.7691','2020-02-19 12:21:52','2020-02-19 12:21:52'),(87,'Cara Kohler','Abshire',20,'rosenbaum.junius@example.com','+14795297661','2020-02-19 12:21:52','2020-02-19 12:21:52'),(88,'Humberto Hand','Fritsch',20,'lubowitz.kiarra@example.org','1-982-251-7371 x398','2020-02-19 12:21:52','2020-02-19 12:21:52'),(89,'Ms. Raina Hahn','Treutel',2,'ostiedemann@example.net','(514) 381-7114','2020-02-19 12:21:52','2020-02-19 12:21:52'),(90,'Elton Schinner','Boyer',28,'marguerite.botsford@example.org','+1.269.843.2584','2020-02-19 12:21:52','2020-02-19 12:21:52'),(91,'Mr. Brown Schimmel DVM','Nitzsche',22,'ygorczany@example.net','+15735701854','2020-02-19 12:21:52','2020-02-19 12:21:52'),(92,'Mariana Moen','Gaylord',3,'adriel02@example.com','207-662-3062 x57174','2020-02-19 12:21:52','2020-02-19 12:21:52'),(93,'Garland Krajcik','Kirlin',24,'cyrus28@example.org','+1 (791) 641-2184','2020-02-19 12:21:52','2020-02-19 12:21:52'),(94,'Ernesto Metz','King',13,'xbrown@example.net','1-403-395-1893','2020-02-19 12:21:52','2020-02-19 12:21:52'),(95,'Carol Batz','Lehner',16,'anderson.vincenza@example.com','+1-856-747-2445','2020-02-19 12:21:52','2020-02-19 12:21:52'),(96,'Ms. Delta Rohan','Kassulke',1,'balistreri.jackson@example.org','708.881.3484','2020-02-19 12:21:52','2020-02-19 12:21:52'),(97,'Madelynn Walter','Wintheiser',10,'augusta30@example.net','(825) 388-1843 x65891','2020-02-19 12:21:52','2020-02-19 12:21:52'),(98,'Danial Kunze','Dickinson',10,'romaguera.pamela@example.net','+1-726-360-1251','2020-02-19 12:21:52','2020-02-19 12:21:52'),(99,'Lamont Kiehn','Ortiz',26,'hwill@example.com','+1-714-664-8693','2020-02-19 12:21:52','2020-02-19 12:21:52'),(100,'Ms. Maegan Bode I','Jaskolski',29,'bjacobi@example.com','(431) 968-6383 x13745','2020-02-19 12:21:52','2020-02-19 12:21:52'),(101,'Milo','Milolo',34,NULL,'asdasdasd','2020-02-20 09:09:35','2020-02-20 09:09:35'),(103,'Paco','Rio',3,'paco@pa.com','asd','2020-02-20 09:36:59','2020-02-20 09:37:13'),(104,'Leonardo','Lucián',22,'leo@leo.com','333222333','2020-02-20 09:40:25','2020-02-20 09:40:35');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_02_19_084550_create_companies_table',1),(5,'2020_02_19_091443_create_employees_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('admin@admin.com','$2y$10$F4V4TSkFjUlwzLIThkKpVOr.2eMPz5xzBorEPlXtW87NB8JCytxB2','2020-02-20 09:51:47');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@admin.com',NULL,'$2y$10$oQ/Wv1Gw0qUbRf2QSpj0BOh8AkNA9.WbJduz4xNuSJ8gek7G4INnK','lBT1hqjSsFWIcyXdkqzLtBTXwNbFmH2uJ6SGqBUbPn73CCPbhVlBuEURUEDy',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-20 12:22:56
