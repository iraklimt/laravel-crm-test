## Project requirements 

#### Project to manage companies and their employees. A small CRM.

- Basic Laravel Auth: ability to log in as administrator
- Use database seeds to create first user with email
admin@admin.com and password “password”
- CRUD functionality (Create / Read / Update / Delete) for two
menu items: Companies and Employees.


- Companies DB table consists of these fields:
  - Name (required)
  - Email
  - logo (minimum 100×100)
  - website
      
      
- Employees DB table consists of these fields:
  - First name (required)
  - last name (required)
  - Company (foreign key to Companies)
  - Email
  - phone


- Use database migrations to create those schemas above
- Store companies logos in storage/app/public folder and make them accessible from public
- Use basic Laravel resource controllers with default methods (index, create, store etc.).
- Use Laravel’s validation function, using Request classes
- Use Laravel’s pagination for showing Companies/Employees list, 10 entries per page
- Use Laravel make:auth as default Bootstrap-based design theme, but remove ability to register

#### Some additional info about the project


- The project is deployed in LEMP stack:
	 - LINUX OS - Ubuntu v 16.04.3 LTS)
	 - NGINX v 1.17.3 LTS
	 - MYSQL v 5.7.27-0ubuntu0.16.04.1
	 - PHP v 7.2.3

- Current Project is made with Laravel Framework 6.16.0
- The MySQL database used for this project is stored in laravel_test/database 
 
[Check out to my personal project](https://drophousing.com)