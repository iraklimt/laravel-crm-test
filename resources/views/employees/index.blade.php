@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <!-- Success message on delete Company  -->
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h1 class="float-left">Employees</h1>
                        <a class="btn btn-outline-success float-right"
                           href="/employees/create" role="button">Add new Employee</a>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Company</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <tr>
                                    <th scope="row">{{$employee->id}}</th>
                                    <td>{{$employee->first_name." ".$employee->last_name}}</td>
                                    <td><a href="/companies/{{$employee->company ?? ''}}">{{$employee->comp->name ?? ''}}</a></td>
                                    <td>
                                        <a class="btn btn-outline-dark btn-sm"
                                           href="/employees/{{$employee->id}}" role="button">show</a>

                                        <a class="btn btn-outline-success btn-sm"
                                           href="/employees/{{$employee->id}}/edit" role="button">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $employees->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
