@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h1 class="card-header">Dashboard</h1>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p class="lead text-muted">¿What do you want to do?</p>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h1 class="card-title">Companies</h1>
                                    <a href="/companies" class="btn btn-primary">Manage Companies</a>
                                    <a href="/companies/create" class="btn btn-outline-success">Add new Company</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h1 class="card-title">Employees</h1>
                                    <a href="/employees" class="btn btn-primary">Manage Employees</a>
                                    <a href="/employees/create" class="btn btn-outline-success">Add new Employee</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
