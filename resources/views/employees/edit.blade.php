@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <!-- Error and success messages -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h1 class="float-left">Edit {{$employee->first_name." ".$employee->last_name}}</h1>
                        <a class="btn btn-link float-right" href="/employees" role="button">Go back</a>
                    </div>

                    <div class="card-body">


                        <form method="POST" action="/employees/{{$employee->id}}">
                            @method('PUT')
                            @csrf
                            <div class="form-group row">
                                <label for="first_name" class="col-sm-2 col-form-label">First name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="first_name" class="form-control" id="first_name"
                                           value="{{$employee->first_name ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="last_name" class="col-sm-2 col-form-label">Last name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="last_name" class="form-control" id="last_name"
                                           value="{{$employee->last_name ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" id="email"
                                           value="{{$employee->email ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" class="form-control" id="phone"
                                           value="{{$employee->phone ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="company" class="col-sm-2 col-form-label">Company</label>
                                @if(!empty($companies))
                                    <div class="col-sm-10">
                                        <select id="company" name="company" class="form-control">
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}"
                                                    {{$company->id == $employee->company ? 'selected' :''}}>
                                                    {{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <div class="alert alert-danger mx-3" role="alert">
                                        No companies available. Please add a new Company first.
                                    </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>

                        <hr>
                        <!-- Remove Company permanently -->
                        <form method="POST" action="/employees/{{$employee->id}}">
                            @method('DELETE')
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-danger float-right">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
