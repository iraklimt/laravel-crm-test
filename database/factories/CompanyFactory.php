<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    $company_name = $faker->company;
    return [
        'name' => $company_name,
        'email' => $faker->unique()->safeEmail,
        'website' => 'www.'.strtolower(preg_replace("/[^a-zA-Z]/", "", $company_name)).'.com',
    ];
});

