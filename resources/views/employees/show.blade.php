@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h1 class="float-left">{{ucfirst($employee->first_name." ".$employee->last_name)}}</h1>
                        <a class="btn btn-link float-right" href="/employees" role="button">Go back</a>
                    </div>

                    <div class="card-body">

                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <p class="card-title">First name: <b>{{ucfirst($employee->first_name)}}</b></p>
                                        <p class="card-text">Last name: <b>{{ucfirst($employee->last_name)}}</b></p>
                                        <p class="card-text">Email: <b>{{$employee->email ?? '---'}}</b></p>
                                        <p class="card-text">Phone: <b>{{$employee->phone ?? '---'}}</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <a class="btn btn-outline-success float-right"
                           href="/employees/{{$employee->id}}/edit" role="button">Edit Employee</a>
                    </div>

                    <hr>

                    @if( empty($employee->comp) )
                        <div class="alert alert-danger m-4" role="alert">
                            This employee does not belong to any company
                        </div>
                    @else
                        <h4 class="float-left mx-4">Company</h4>

                        <div class="card-body">
                            <div class="card mb-3">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                        <img src="{{asset($employee->comp->logo ?? 'storage/no-logo.png')}}"
                                             class="card-img img-fluid" alt="company logo">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-title">Name: <b>{{$employee->comp->name}}</b></p>
                                            <p class="card-text">Contact: <b>{{$employee->comp->email ?? '---'}}</b></p>
                                            <p class="card-text">Website:
                                                <a href="{{'http://'.$employee->comp->website ?? ''}}">
                                                    {{$employee->comp->website ?? ''}}</a></p>

                                            <p class="card-text">Total Employees:
                                                <b>{{$employee->comp->employees->count()}}</b></p>

                                            <a class="btn btn-outline-info"
                                               href="/companies/{{$employee->comp->id}}"
                                               role="button">Show Company</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection
