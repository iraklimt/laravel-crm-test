@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h1 class="float-left">{{$company->name}}</h1>
                        <a class="btn btn-link float-right" href="/companies" role="button">Go back</a>
                    </div>

                    <div class="card-body">

                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="{{asset($company->logo ?? 'storage/no-logo.png')}}"
                                         class="card-img img-fluid" alt="company logo">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <p class="card-title">Name: <b>{{$company->name}}</b></p>
                                        <p class="card-text">Contact: <b>{{$company->email ?? '---'}}</b></p>
                                        <p class="card-text">Website: <a href="{{'http://'.$company->website ?? ''}}">
                                            {{$company->website ?? ''}}</a></p>
                                        <p class="card-text">Total Employees: <b>{{$company->employees->count()}}</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(!empty($company->employees) && $company->employees->count() > 0)
                            <h2>Employees</h2>
                            @foreach($company->employees->all() as $employee)
                                <div class="list-group">
                                    <a href="/employees/{{$employee->id}}"
                                       class="list-group-item list-group-item-action">
                                        {{$employee->first_name}}
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-danger" role="alert">
                                This company does'nt have employees
                            </div>
                        @endif
                        <br>
                        <a class="btn btn-outline-success float-right"
                           href="/companies/{{$company->id}}/edit" role="button">Edit Company</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
