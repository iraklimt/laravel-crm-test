<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'nullable|email',
            'website' => 'nullable',
            'logo' => 'nullable|image|dimensions:min_width=100,min_height=100',
        ]);

        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        if($request->file('logo')){
            $path = $request->file('logo')->store('public');
            $public_path = str_replace('public/', 'storage/', $path);
            $company->logo = $public_path;
        }

        $company->save();

        return redirect('companies/'.$company->id)->with('success','Company successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'nullable|string',
            'email' => 'nullable',
            'website' => 'nullable',
            'logo' => 'nullable|image|dimensions:min_width=100,min_height=100',
        ]);

        !empty($request->name) ? $company->name = $request->name : null;
        !empty($request->email) ? $company->email = $request->email : null;
        !empty($request->website) ? $company->website = $request->website : null;

        if($request->file('logo')){
            $path = $request->file('logo')->store('public');
            $public_path = str_replace('public/', 'storage/', $path);
            $company->logo = $public_path;
        }

        $company->save();

        return redirect()->back()->with('success','Company successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect('/companies')->with('success','Company successfully deleted');
    }
}
